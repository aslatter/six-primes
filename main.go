package main

import (
	"fmt"
	"math/big"
	"math/rand/v2"
)

const (
	MIN = 100000
	MAX = 999999
)

func main() {
	// bit-set of composite integers
	var compositeMap big.Int
	// found primes
	var primes []int

	// https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
	for i := 2; i <= MAX; i++ {
		// is this value already flagged as composite?
		if compositeMap.Bit(i) == 1 {
			continue
		}
		// mark down-stream values as composite
		// (we don't need to mark (i * 2), (i * 3), (i * 5), etc because those
		// are also multiples of 2, 3, and 5 and so would already have been marked).
		for j := i - 1; i+(j*i) <= MAX; j++ {
			compositeMap.SetBit(&compositeMap, i+(j*i), 1)
		}
		// collect found primes
		if i >= MIN {
			primes = append(primes, i)
		}
	}

	rand.Shuffle(len(primes), func(i, j int) {
		primes[i], primes[j] = primes[j], primes[i]
	})

	for _, n := range primes {
		fmt.Println(n)
	}
}
